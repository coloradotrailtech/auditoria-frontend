import Cookies from "js-cookie";

export default (to, from, next) => {
  const token = Cookies.get("token");
  if (!token) { //añadir otra validacion para cuando el token no es valido redireccionar a Login tambien
    next({ name: "Login" });
    return false;
  }
};
