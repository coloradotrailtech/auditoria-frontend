export const defaultState = function() {
    return {
        //Action getProvincias
        getProvinciasLoading: false,
        getProvinciasSuccess: false,
        getProvinciasError: false,
        getProvinciasErrorMessage: '',
        provincias:[],
    }
}