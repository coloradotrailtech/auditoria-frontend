import { defaultState } from "./config";
import Vue from "vue";

export const mutations = {
  RESET(state) {
    const s = defaultState();
    Object.keys(s).forEach((key) => {
      state[key] = s[key];
    });
    state.email= null;
    state.cliente= null;
    state.status= null;
    state.isAdmin= false;
  },

  LOGIN_USER_SUCCESS(state, payload) {
    console.log("payload", payload)
    console.log("state", state)
    state.login = true;
    state.getUserLoading = false;
    state.getUserSuccess = true;
    state.isAdmin = payload.cliente ? false : true;
    state.email = payload.email;
    state.cliente = payload.cliente;
    state.status = payload.status;
  },

  LOGIN_USER_ERROR(state, e) {
    Vue.swal({
      title: "Credenciales de acceso incorrectas",
      text: "por favor verifique su email y contraseña proporcionados",
      type: "warning",
      timer: 3500,
    });
    state.getUserLoading = false;
    state.getUserError = true;
    state.state.login = false;
    state.getUserErrorMessage = e;
  },


  GET_USER_SUCCESS(state, user) {
    state.getUserLoading = false;
    state.getUserSuccess = true;
    state.user = user;
  },

  GET_USER_ERROR(state, e) {
    state.getUserLoading = false;
    state.getUserError = true;
    state.getUserErrorMessage = e;
  },

  REMOVE_USER_SUCCESS(state) {
    state.removeUserLoading = false;
    state.removeUserSuccess = true;
  },

  REMOVE_USER_LOADING(state) {
    state.removeUserLoading = true;
    state.removeUserSuccess = false;
    state.removeUserError = false;
    state.removeUserErrorMessage = "";
  },

  REMOVE_USER_ERROR(state, e) {
    state.removeUserLoading = false;
    state.removeUserError = true;
    state.removeUserErrorMessage = e;
  },

  UPDATE_USER_SUCCESS(state) {
    state.updateUserThemeLoading = false;
    state.updateUserThemeSuccess = true;
  },

  UPDATE_USER_ERROR(state, e) {
    state.updateUserLoading = false;
    state.updateUserError = true;
    state.updateUserErrorMessage = e;
    Vue.swal({
      title: "Error",
      text: "algo fallo durante la actualización",
      type: "error",
      width: "350px",
      timer: 2000,
      position: "top-end",
    });
  },
};
