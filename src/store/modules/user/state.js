const state = Object.assign(
    {
        email: null,
        cliente: null,
        status: null,
        isAdmin: false,
        login: null
    }, 
);

export default state;