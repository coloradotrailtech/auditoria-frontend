export const defaultState = function() {
    return {
        //Action getCategoriasAfip
        getCategoriasAfipLoading: false,
        getCategoriasAfipSuccess: false,
        getCategoriasAfipError: false,
        getCategoriasAfipErrorMessage: '',
        categoriasAfip:[],
    }
}