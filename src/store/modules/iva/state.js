import { defaultState } from './config'; 

const state = Object.assign({}, defaultState());

export default state;