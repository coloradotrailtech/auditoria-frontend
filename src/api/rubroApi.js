import api from './api';
import Cookies from "js-cookie";

const rubroApi = {
    getRubros() {
        return api
            .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .get(`http://${process.env.VUE_APP_IP}:4000/api/rubros`);   //Rubro
    },
    removeRubro(id) {
        return api.axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .delete(`http://${process.env.VUE_APP_IP}:4000/api/rubros/${id}`);
    },

    postRubro({
        nombre,
        controlContratista,
    }) {
        return api
            .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .post(`http:///${process.env.VUE_APP_IP}:4000/api/rubros`, {
                nombre,
                controlContratista,
            });
    },

    updateRubro({
        id,
        nombre,
        controlContratista,
    }) {
        return api.axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .put(`http://${process.env.VUE_APP_IP}:4000/api/rubros/${id}`, {
                nombre,
                controlContratista,
            });
    },
};

export default rubroApi;