import api from "./api";
import Cookies from "js-cookie";

const empleadoApi = {
  getEmpleados() {
     return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .get(`http://${process.env.VUE_APP_IP}:4000/api/empleados`); / //Empleado
  },

  getEmpleadosCliente(cliente_id) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .get(
        `http://${process.env.VUE_APP_IP}:4000/api/empleados/cliente/${cliente_id}`
      );
  },

  postEmpleado({
    nombre,
    apellido,
    fechadeingreso,
    tipo,
    documento,
    cliente,
    sucursal
  }) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .post(`http://${process.env.VUE_APP_IP}:4000/api/empleados`, {
        nombre,
        apellido,
        fechadeingreso,
        tipo,
        documento,
        cliente_id: cliente._id,
        sucursal_id: sucursal._id,
      });
  },

  removeEmpleado(id) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .delete(`http://${process.env.VUE_APP_IP}:4000/api/empleados/${id}`);
  },

  updateEmpleado({
    id,
    nombre,
    apellido,
    fechadeingreso,
    tipo,
    documento,
    cliente,
    sucursal
  }) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .put(`http://${process.env.VUE_APP_IP}:4000/api/empleados/${id}`, {
        nombre,
        apellido,
        fechadeingreso,
        tipo,
        documento,
        cliente_id: cliente._id,
        sucursal_id: sucursal._id,
      });
  },

};

export default empleadoApi;
