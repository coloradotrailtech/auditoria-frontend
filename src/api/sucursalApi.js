import api from './api';
import Cookies from "js-cookie";

const sucursalApi = {
    getSucursales(){
        return api.axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
        .get(`http://${process.env.VUE_APP_IP}:4000/api/sucursales`); //Sucursal
    },
    
    postSucursal({
       direccion, 
       cliente, 
       telefono, 
       localidad,
    })  {
        return api
        .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
        .post(`http://${process.env.VUE_APP_IP}:4000/api/sucursales`,{
          direccion, 
          cliente_id: cliente._id, 
          telefono, 
          localidad_id: localidad._id,
        });
    },

    updateSucursal({
      id, 
      direccion, 
      cliente, 
      telefono, 
      localidad, 
    }) {
        return api
        .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
        .put(`http://${process.env.VUE_APP_IP}:4000/api/sucursales/${id}`, {
            direccion, 
            cliente_id: cliente._id, 
            telefono, 
            localidad_id: localidad._id, 
        });
    },

    removeSucursal(id){
        return api
            .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .delete(`http://${process.env.VUE_APP_IP}:4000/api/sucursales/${id}`);
    }
};

export default sucursalApi;