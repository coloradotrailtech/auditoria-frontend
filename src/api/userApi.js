import api from "./api";
import Cookies from "js-cookie";

const userApi = {
  async getUser(id) {
    return await api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .get(`http://${process.env.VUE_APP_IP}:4000/api/users/${id}`);
  },

  removeUser(id) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .delete(`http://${process.env.VUE_APP_IP}:4000/api/users/${id}`);
  },

  updateUser({ id, password, status }) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .put(`http://${process.env.VUE_APP_IP}:4000/api/users/${id}`, {
        password,
        status,
      });
  },
};

export default userApi;
