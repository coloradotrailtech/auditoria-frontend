import api from "./api";
import Cookies from "js-cookie";

const clienteApi = {
  getClientes() {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .get(`http://${process.env.VUE_APP_IP}:4000/api/clientes`); //Cliente
  },

  postCliente({
    nombreFantasia,
    razonSocial,
    cuit,
    email,
    telefono,
    pais,
    categoriaAfip,
    rubro,
    image,
    active,
  }) {
    let formData = new FormData();
    formData.append("nombreFantasia", nombreFantasia);
    formData.append("razonSocial", razonSocial);
    formData.append("cuit", cuit);
    formData.append("email", email);
    formData.append("telefono", telefono);
    formData.append("pais_id", pais._id);
    formData.append("categoriaAfip_id", categoriaAfip._id);
    formData.append("rubro_id", rubro._id);
    formData.append("image", image);
    formData.append("active", active);

    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .post(`http://${process.env.VUE_APP_IP}:4000/api/clientes`, formData);
  },

  removeCliente(id) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .delete(`http://${process.env.VUE_APP_IP}:4000/api/clientes/${id}`);
  },

  updateCliente({
    id,
    nombreFantasia,
    razonSocial,
    cuit,
    email,
    telefono,
    pais,
    rubro,
    categoriaAfip,
    image,
    filename
  }) {
    let formData = new FormData();
    formData.append("nombreFantasia", nombreFantasia);
    formData.append("razonSocial", razonSocial);
    formData.append("cuit", cuit);
    formData.append("email", email);
    formData.append("telefono", telefono);
    formData.append("pais_id", pais._id);
    formData.append("categoriaAfip_id", categoriaAfip._id);
    formData.append("rubro_id", rubro._id);
    formData.append("image", image);
    formData.append("oldfilename", filename);
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .put(
        `http://${process.env.VUE_APP_IP}:4000/api/clientes/${id}`,
        formData
      );
  },

  updateClienteStatus({ id, active }) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .put(`http://${process.env.VUE_APP_IP}:4000/api/clientes/${id}`, {
        active,
      });
  },

  deactivateCliente(id) {
    console.log(id, "cliente id desde backend");
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .patch(`http://${process.env.VUE_APP_IP}:4000/api/clientes/${id}`, {
        active: false,
      });
  },
};

export default clienteApi;