import api from "./api";
import Cookies from "js-cookie";

const ordenApi = {
  getOrdenes() {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .get(`http://${process.env.VUE_APP_IP}:4000/api/ordenes`);
  },

  postOrden({
    sucursal,
    tipoOrden,
    fechainicio,
    fechafin,
    fechavencimiento,
  }) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .post(`http://${process.env.VUE_APP_IP}:4000/api/ordenes`, {
        tipoOrden_id: tipoOrden._id,
        sucursal_id: sucursal._id,
        fechainicio,
        fechafin,
        fechavencimiento,
      });
  },

  removeOrden(id) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .delete(`http://${process.env.VUE_APP_IP}:4000/api/ordenes/${id}`);
  },

  updateOrden({
    id,
    sucursal,
    tipoOrden,
    fechainicio,
    numeroorden,
    fechafin,
    fechavencimiento,
  }) {
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .put(`http://${process.env.VUE_APP_IP}:4000/api/ordenes/${id}`, {
        tipoOrden_id: tipoOrden._id,
        sucursal_id: sucursal._id,
        fechainicio,
        numeroorden,
        fechafin,
        fechavencimiento,
      });
  },

  postCapacitacion({
    tipoOrden,
    ispresencial,
    horainicio,
    horafin,
    planilla,
    attendees,
    sucursal,
    fechainicio,
    fechafin,
    fechavencimiento,
  }) {
    let formData = new FormData();
    formData.append("tipoOrden_id", tipoOrden._id);
    formData.append("ispresencial", ispresencial);
    formData.append("horainicio", horainicio);
    formData.append("horafin", horafin);
    formData.append("planilla", planilla);
    formData.append("attendees", JSON.stringify(attendees));
    formData.append("sucursal_id", sucursal._id);
    formData.append("cliente_id", sucursal.cliente_id);
    formData.append("fechainicio", fechainicio);
    formData.append("fechafin", fechafin);
    formData.append("fechavencimiento", fechavencimiento);
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .post(
        `http://${process.env.VUE_APP_IP}:4000/api/capacitaciones`,
        formData
      );
  },

  updateCapacitacion({
    id,
    orden_id,
    tipoOrden,
    ispresencial,
    horainicio,
    horafin,
    numeroorden,
    planilla,
    attendees,
    sucursal,
    fechainicio,
    fechafin,
    fechavencimiento,
    filename,
  }) {
    let formData = new FormData();
    formData.append("ispresencial", ispresencial);
    formData.append("orden_id", orden_id);
    formData.append("horainicio", horainicio);
    formData.append("horafin", horafin);
    formData.append("numeroorden", numeroorden);
    formData.append("attendees", JSON.stringify(attendees));
    formData.append("tipoOrden_id", tipoOrden._id);
    formData.append("sucursal_id", sucursal._id);
    formData.append("cliente_id", sucursal.cliente_id);
    formData.append("fechainicio", fechainicio);
    formData.append("fechafin", fechafin);
    formData.append("fechavencimiento", fechavencimiento);
    formData.append("planilla", planilla);
    formData.append("oldfilename", filename);
    return api
      .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
      .put(
        `http://${process.env.VUE_APP_IP}:4000/api/capacitaciones/${id}`,
        formData
      );
  },
};

export default ordenApi;
