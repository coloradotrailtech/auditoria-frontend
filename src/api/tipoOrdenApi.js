import api from './api';
import Cookies from "js-cookie";

const tipoOrdenApi = {
    getTiposOrdenes() {
        return api
          .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
          .get(`http://${process.env.VUE_APP_IP}:4000/api/tiposOrden`); //TipoOrden
    },

    postTipoOrden({
        nombre,
        capacitacion,
    }) {
        return api
            .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .post(`http://${process.env.VUE_APP_IP}:4000/api/tiposOrden`, {
                nombre,
                capacitacion,
            });
    },

    removeTipoOrden(id) {
        return api
           .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
           .delete(`http://${process.env.VUE_APP_IP}:4000/api/tiposOrden/${id}`);
    },

    updateTipoOrden({
        id,
        nombre,
        capacitacion,
    }) {
        return api
            .axiosInstance(null, null, { "x-access-token": Cookies.get("token") })
            .put(`http://${process.env.VUE_APP_IP}:4000/api/tiposOrden/${id}`, {
                nombre,
                capacitacion,
            });
    },


};

export default tipoOrdenApi;