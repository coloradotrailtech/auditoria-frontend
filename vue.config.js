module.exports = {
  devServer: {
    disableHostCheck: true,
    proxy: 'http://localhost:4000/',
  }
}
